;Log.au3 created by RunzelEier
;Version 1.0
;Released 11.01.2014


#include-once
;needed variables
;you should not change these
Global $__LogFileHandle
Global $__LogWriteToFile = False
Global $__LogFilePath = "Log.txt"
Global $__LogLevel = 6

Global Const $__LogERROR = 1
Global Const $__LogWARNING = 2
Global Const $__LogINFO = 3
Global Const $__LogDEBUG = 4
Global Const $__LogVERBOSE = 5

Global Const $__LogERROR_PREFIX = "!"
Global Const $__LogWARNING_PREFIX = "-"
Global Const $__LogINFO_PREFIX = ">"
Global Const $__LogDEBUG_PREFIX = "+"
Global Const $__LogVERBOSE_PREFIX = " "

Global Const $__LogDefaultMessage = "no Message"

Func LogClose()
	if $__LogFileHandle <> 0 Then
		FileClose($__LogFileHandle)
	EndIf
EndFunc

Func LogSetLogToFile($bLogToFile)
	$__LogWriteToFile = $bLogToFile
EndFunc

Func LogSetLogFile($sFilePath)
	$__LogFilePath = $sFilePath
EndFunc

Func LogSetLogLevel($iLogLevel)
	$__LogLevel = $iLogLevel
EndFunc

Func LogE($sMessage, $SkriptName = "", $SkriptLine = "")
	LogError($sMessage, $SkriptName, $SkriptLine)
EndFunc

Func LogError($sMessage, $SkriptName = "", $SkriptLine = "")
	__Log($sMessage,$__LogERROR, $__LogERROR_PREFIX, $SkriptName, $SkriptLine)
EndFunc

Func LogW($sMessage, $SkriptName = "", $SkriptLine = "")
	LogWarning($sMessage, $SkriptName, $SkriptLine)
EndFunc

Func LogWarning($sMessage, $SkriptName = "", $SkriptLine = "")
	__Log($sMessage,$__LogWARNING, $__LogWARNING_PREFIX, $SkriptName, $SkriptLine)
EndFunc

Func LogI($sMessage, $SkriptName = "", $SkriptLine = "")
	LogInfo($sMessage, $SkriptName, $SkriptLine)
EndFunc

Func LogInfo($sMessage, $SkriptName = "", $SkriptLine = "")
	__Log($sMessage,$__LogINFO, $__LogINFO_PREFIX, $SkriptName, $SkriptLine)
EndFunc

Func LogD($sMessage, $SkriptName = "", $SkriptLine = "")
	LogDebug($sMessage, $SkriptName, $SkriptLine)
EndFunc

Func LogDebug($sMessage, $SkriptName = "", $SkriptLine = "")
	__Log($sMessage,$__LogDEBUG, $__LogDEBUG_PREFIX, $SkriptName, $SkriptLine)
EndFunc

Func LogV($sMessage, $SkriptName = "", $SkriptLine = "")
	LogVerbose($sMessage, $SkriptName, $SkriptLine)
EndFunc

Func LogVerbose($sMessage, $SkriptName = "", $SkriptLine = "")
	__Log($sMessage,$__LogVERBOSE, $__LogVERBOSE_PREFIX, $SkriptName, $SkriptLine)
EndFunc


;internal functions which are not meant for direct call


Func __Log($sMessage, $iLevel, $sPrefix = "", $SkriptName = "", $SkriptLine = "")
	Local $sLevel = ""
	if $__LogLevel <= $iLevel Then Return
	Switch $iLevel
		Case $__LogERROR
			$sLevel = "ERROR"
		Case $__LogWARNING
			$sLevel = "WARNING"
		Case $__LogINFO
			$sLevel = "INFO"
		Case $__LogDEBUG
			$sLevel = "DEBUG"
		Case $__LogVERBOSE
			$sLevel = "VERBOSE"
	EndSwitch
	$sFormatedMessage = StringFormat("%s### %7s ### [%02i:%02i:%02i] ",$sPrefix,$sLevel, @HOUR, @MIN, @SEC)
	if $SkriptName <> "" Then
		$sFormatedMessage &= StringFormat("Skriptname: %s ",$SkriptName)
	EndIf
	if $SkriptLine <> "" Then
		$sFormatedMessage &= StringFormat("SkriptLine: %s ",$SkriptLine)
	EndIf
	$sFormatedMessage &= StringFormat("Message: %s \n",$sMessage)

	ConsoleWrite($sFormatedMessage)
	if $__LogWriteToFile Then
		if $__LogFileHandle = 0 Then
			__LogInit()
		EndIf
		FileWriteLine($__LogFileHandle,$sFormatedMessage)
	EndIf
EndFunc

Func __LogInit()
	$__LogFileHandle = FileOpen($__LogFilePath,2)
EndFunc